import React, {Component} from 'react';
import {Button, Form, Input} from 'antd';
import './login.css'
import {signin} from "../../server/auth/authRequest";
import {getToken, setLocalStorage} from "../../utilits";
import {Redirect} from "react-router-dom";
import Home from "../home/home";
import Header from "../header/header";
import App from "../../App";


export default class Login extends Component {
    state = {
        isUser: false,
        loading: false
    }

    onLoading = () => {
        this.setState({
            loading: true
        })
    }

    render() {
        const onFinish = (values) => {
            this.onLoading()
            signin(values).then(res => {
                if (res && res.data && res.data.token) {
                    setLocalStorage('token', res.data.token);
                    this.setState({
                        isUser: true,
                        loading: false
                        }
                    )
                    window.location.reload();
                }
            }).catch(err => {
                alert(err)
                this.setState({
                    loading: false
                })
            })
        }

        return (
            <React.Fragment className="pg1">
                {
                    getToken() ?
                        <div>
                            <Redirect to=''/>
                            <App/>
                        </div> :
                        <Form
                            name="basic"
                            initialValues={{remember: true}}
                            className="registerForm"
                            onFinish={onFinish}
                        >
                            
                            <Form.Item
                                //label="Логин"
                                name="username"
                                rules={[{required: true, message: 'Введите логин!'}]}
                            >
                                <Input className="inpt" placeholder="Введите логин"/>
                            </Form.Item>

                            <Form.Item
                                //label="Парол"
                                name="password"
                                rules={[{required: true, message: 'Введите парол'}]}
                            >
                                <Input.Password className="inpt" placeholder="Введите парол"/>
                            </Form.Item>


                            <Form.Item>
                                <Button loading={this.state.loading} type="primary" htmlType="submit" className="btn1">
                                    Вход в систему
                                </Button>
                            </Form.Item>
                        </Form>
                }
            </React.Fragment>
        )
    }
}